function M = MIC_ARRAY_NBF()
    % mic_array_NBF return parameters for Natural B-format mic array
    
    M.name = 'NBF';
    
    M.alpha = [ ...
        1 0;
        0 1;
        0 1;
        0 1;
        ];
    
    
    % capsule orientations, as direciton cosines
    M.o = [ ...
        +1  0  0; % omni, capsule orientaiton does matter
        +1  0  0; % +X
         0 +1  0; % +Y
         0  0 +1; % +Z
        ];
    % normalize
    M.o = diag(1./sqrt(diag(M.o * M.o'))) * M.o;
    
    % capsule positions, in meters
    M.u = [ ...
         0  0  0; % omni
         0  0  0; % +X
         0  0  0; % +Y
         0  0  0; % +Z
        ];
    
    % a2b matrix, Batke calls this the mode matrix, \Psi.
    % It has one column for each capsule, one row for each output.
    % In this case it is the indentity matrix because we want
    % the capsule signals directly.  Note this can also be given as
    % eye(M.alpha,1);
    M.a2b = [ ...
         1  0  0  0; % omni
         0  1  0  0; % +X
         0  0  1  0; % +Y
         0  0  0  1; % +Z
        ];
end
