function M = MIC_ARRAY_Tetra(radius, alpha1)
    % mic_array_tetra return parameters for generic tetra microphone
    %  RADIUS is the acoustic radius of the array, typically 10% larger
    %  than physical radius.
    %  ALPHA1 is the gain of the first-order parameter
    %   0 = omni, 
    %   1/2 = cardioid, 
    %   1 = fig-8
    %   (3-sqrt(3))/2 = supercardioid (~0.63397)
    %   3/4 = hypercardoid
    %
    % The default values for RADIUS and ALPHA1 give the results shown in
    % Gerzon, "The Design of Precisely Coincident Microphone Arrays for
    % Stereo and Surround Sound", 50th AES Convention, London, 1975.
    
    M.name = 'Tetra';
    
    % Soundfield mic is four mics mounted on the faces of a tetrahedron.
    if ~exist('radius', 'var')
        M.r = 1.47 * 1e-2; % meters
    else
        M.r = radius;
    end
    
    % capsule directivities
    %      for first-order mic: omni=[1,0]; cardioid=[0.5,0.5],fig8=[0,1];
    if ~exist('alpha1', 'var')
        alpha1 = 0.5;
    end
    
    M.alpha = [ ...
        1-alpha1 alpha1;
        1-alpha1 alpha1;
        1-alpha1 alpha1;
        1-alpha1 alpha1; ];
    
    
    % capsule orientations, direciton cosines
    M.o = [ ...
        -1  1 -1; % back-left-down .... LBD
        +1  1  1; % front-left-up ..... LFU
        +1 -1 -1; % front-right-down .. RFD
        -1 -1  1; % back-right-up ..... RBU
        ];
    % normalize
    M.o = diag(1./sqrt(diag(M.o * M.o'))) * M.o;
    
    % capsule positions for radial array, in meters
    M.u = M.o * M.r;
    
    % a2b matrix, Batke calls this the mode matrix, \Psi
    M.a2b = [ones(size(M.o,1),1), sign(M.o)]';
end
