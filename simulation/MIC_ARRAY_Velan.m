%% Umashankar's Velan
function M = MIC_ARRAY_Velan()
 
    M.name = 'Velan';
    
    % Umashankar's Velan is four TSB-140 cardioids with pairs pointed along
    % axis with separation 14.3 mm.  See
    %    http://www.shapeways.com/model/143678/velan-140-internals.html
    %    http://www.firstpr.com.au/rwi/mics/2009-09-a/2011052109473129207TSB-140A25-GP.jpg
    % TSB-140 is 14mm diameter x 6.3mm depth.  I assume the rings are 1 mm
    % thick.  Acoustic radius is typically 20% larger than physical.
    M.r = 1.2*(14+2+2*6.3)/2 * 1e-3; % meters
    
    % capsule directivities
    %      for first-order mic: omni=[1,0]; cardioid=[0.5,0.5],fig8=[0,1];
    M.alpha = [ ...
        0.5 0.5;
        0.5 0.5;
        0.5 0.5;
        0.5 0.5;
        0.5 0.5;
        0.5 0.5 ];
    
    % capsule orientations, direciton cosines
    % a row for each capsule, columns are x,y,z
    M.o = [ ...
        1  0  0;
       -1  0  0;
        0  1  0;
        0 -1  0;
        0  0  1;
        0  0 -1];
    
    if false
        % specify in spherical (az, el, r), like this
        M.o = [...
              0,  0, 1;
            180,  0, 1;
             90,  0, 1;
            -90,  0, 1;
              0, 90, 1;
              0,-90, 1 ];
        % convert to radians
        M.o(:,1:2) = M.o(:,1:2)*pi/180;
        % convert to cartesian
        [M.o(:,1), M.o(:,2), M.o(:,3)] = ...
            sph2cart(M.o(:,1),M.o(:,2),M.o(:,3));
    end
    
    % capsule positions for radial array, in meters
    M.u = M.o .* M.r;
    
    % a2b matrix, Batke calls this the mode matrix, \Psi
    % a column for each capsule, a row for each array output
    M.a2b = [ ...
        0.2357    0.2357    0.2357    0.2357    0.2357    0.2357;
        1.0000   -1.0000    0.0000    0.0000    0.0000    0.0000;
        0.0000    0.0000    1.0000   -1.0000    0.0000    0.0000;
        0         0         0         0         1.0000   -1.0000];
end
