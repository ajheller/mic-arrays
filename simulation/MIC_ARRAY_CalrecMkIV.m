%% Calrec MkIV
function M = MIC_ARRAY_CalrecMkIV()
    % original Calrec capsules 10.5 dB f/b ratio
    M = MIC_ARRAY_Tetra(1.47e-2, 0.35073087);
    M.name = 'Calrec_MkIV';
end
