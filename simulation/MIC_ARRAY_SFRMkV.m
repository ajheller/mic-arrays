%% Soundfield MkV
function M = MIC_ARRAY_SFRMkV()
    % new capsules used by Sounfield Research, 9.5 dB f/b ratio
    M = MIC_ARRAY_Tetra(1.47e-2, 0.33251727);
    M.name = 'Soundfield_Research_MkV';
end
