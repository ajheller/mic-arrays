%% Danish Pro Audio DPA-4
function M = MIC_ARRAY_DPA4()
    % DPA-4 array
    M = MIC_ARRAY_Tetra(2.5e-2, 0.5);
    M.name = 'DPA-4';
    
    % DPA uses different A format order and alternate tetrahedral config
    % M.a2b = ???????  (get data from Eric, fill in here )
end
