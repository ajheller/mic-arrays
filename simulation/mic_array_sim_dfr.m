%% Simulate the Diffuse Field Response of Microphone Array
%     Aaron J. Heller (heller@ai.sri.com)
%     28-May-2012, revised 22-Jun-2012, 12-Jan-2013
%     tested with MATLAB R2012b and GNU Octave, version 3.2.4

%% Main function for simulator
function [dfr, ffr, freqs, x] = mic_array_sim_dfr( M, output_file )
    % MIC_ARRAY_SIM_DFR simulate diffuse field response pf mic array M
    %
    %  M.name ... name of array
    %  M.o    ... orientaton of capsules (direction cosines)
    %  M.u    ... positions of capsules (in meters)
    %  M.alpha .. directivities of capsules, by order
    %  M.a2b  ... the A-to-B matrix
    %             (Batke and Elko call this the mode matrix, \Psi)
    %    see MIC_ARRAY_Tetra and _Velan functions below for examples
    %
    % returns:
    %  dfr: diffuse-field response
    %   index 1: frequency (see freqs)
    %   index 2: output (e.g., WXYZ)
    %  ffr: complex free-field response
    %   index 1: frequency (see freqs)
    %   index 2: output (e.g., WXYZ)
    %   index 3: test direction (see x)
    %  freqs: array of frequences used
    %  x: 3xn array of test directions (as column vectors)
    
    %% array to be simulated, all array parameteters in structure M
    if ~exist('M', 'var')
        M = MIC_ARRAY_CalrecMkIV;
    end
    %display(M.name);
    %% Physical constants and simulation parameters
    
    % speed of sound
    c = 340.29; % meters/sec
    
    % number of frequencies for simulation
    n_freqs = 200;
    low_freq = 100;
    high_freq = 20000;
    
    % log or linear
    log_freqs = false;
    
    % file for output, set to false for no file
    if ~exist('output_file', 'var')
        output_file = ['sim_output_', M.name, '.csv'];
    end
    
    % draw a graph?
    graph_response = true;
    
    
    %% Integration on the Sphere
    % use order 20 Lebedev-Laikov quadrature
    a = LebedevGrid(20);
    
    % excitation directions,
    x = [a.x'; a.y'; a.z'];
    
    % number of outputs is number of rows in a2b matrix
    n_Bout = size(M.a2b,1);
    
    % quadrature weights, a.w is for full sphere, normalize to 1, duplicate
    % for each output
    qw = a.w(:,ones(1,n_Bout))' / (4*pi);
    
    % projections of capsule positions along the excitation directions
    d = M.u * x;
    
    % cosine of angle between capsule orientation and excitation directions
    cos_theta = (M.o * x);
    size_cos_theta = size(cos_theta); % for octave
    
    % directional gain for order i is the Legendre Polynomial, P_i
    % evaluated at cos_theta
    dir_gain = diag(M.alpha(:,1)) * ones(size(cos_theta)); % P_0(x) = 1
    for i = 2:size(M.alpha,2)
        if false
            P_i = legendre(i-1, cos_theta);
            P_i = squeeze(P_i(1,:,:));
        else % for octave
            P_i = reshape(legendre(i-1, cos_theta(:)'), [i,size_cos_theta]);
            P_i = squeeze(P_i(1,:,:));
        end
        dir_gain = dir_gain + diag(M.alpha(:,i)) * P_i;
    end
    
    % allocate array to hold diffuse-field frequency response
    dfr = zeros(n_freqs, n_Bout);
    
    % allocate array to hold free-field frequency response
    ffr = zeros(n_freqs, n_Bout, length(x(1,:)));
    
    % test frequencies
    if log_freqs
        freqs = logspace(log10(low_freq), log10(high_freq), n_freqs);
    else
        freqs = linspace(low_freq, high_freq, n_freqs);
    end
    
    i = 1;
    for k = freqs * (2*pi)/c % k is wavenumber
        % compute B-format outputs at this frequency
        B = M.a2b * ( dir_gain .* exp( +1j * k * d ) );
        ffr(i,:,:) = B;
        
        % compute RMS responses over surface of sphere
        dfr(i,:) = sqrt( sum(B .* conj(B) .* qw, 2) );
        i = i + 1;
    end
    
    %% plot and save
    
    % indexes of free-field responses to write out
    %ffr_save = 1:6;
    % cardinal dirs in the X-Y plane
    ffr_save_h = find(mod(a.theta, pi/4) == 0 & a.phi == 0);
    % +/- Z
    ffr_save_v = find(mod(a.phi, pi) == pi/2);
    
    ffr_save = [ffr_save_h; ffr_save_v];
    
    %% save results
    if output_file
        save(output_file, 'x', 'freqs', 'dfr', 'ffr');
    end
    
    %% graph results
    
    if graph_response
        % Graph it
        % dfr(:,1) is W, dfr(:,2) is X, dfr(:,4) is Z
        createfigure(M, freqs, 20*log10(dfr(:,[1,2,4])));
        
        fig = figure(2);
        for i = 1:length(ffr_save)
            subplot(3,4,i)
            if false
            createfigure_ffr(M, x(:,ffr_save(i)), freqs, ...
                20*log10(abs(ffr(:,:,ffr_save(i)))),...
                fig);
            else
                plot(freqs, 20*log10(abs(ffr(:,:,ffr_save(i)))));
                ylim([-12,+6]);
                % Create xlabel
                xlabel('Frequency (Hz x 10^4)', 'FontAngle', 'italic');
                % Create ylabel
                ylabel('Relative Response (dB)', 'FontAngle', 'italic');
                % Create legend
                %legend({'W', 'X', 'Y', 'Z'})
                xdir = x(:,ffr_save(i));
                [az,el,~] = cart2sph(xdir(1),xdir(2),xdir(3));
                title(sprintf('az=%d el=%d', az*180/pi, el*180/pi));
                grid on
                if i == length(ffr_save)
                    legend({'W', 'X', 'Y', 'Z'})
                end
            end
        end
    end
end

%% Plotting functions
function createfigure(M, X1,YMatrix1)
    %  X1:  vector of x data
    %  YMATRIX1:  matrix of y data
    
    %  Auto-generated by MATLAB on 12-Mar-2011 11:02:34
    
    % Create figure
    figure1 = figure();
    
    % Create axes
    axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on',...
        'XMinorTick','on','YMinorTick','on','FontSize',12);
    box(axes1,'on');
    hold(axes1,'all');
    
    % Create multiple lines using matrix input to plot
    plot1 = plot(X1,YMatrix1,'Parent',axes1);
    set(plot1(1),'DisplayName','W');
    set(plot1(2),'DisplayName','XY');
    set(plot1(3),'DisplayName','Z');
    
    % Create title
    name_sanitized = strrep(M.name, '_', '\_');
    if false
        title(sprintf(...
            ['Diffuse Field Response of simulated %s Mic Array' ...
            '\n(r=%0.3fcm, \\alpha=%0.3f)'], ...
            name_sanitized, M.r*100, M.alpha),...
            'FontWeight','bold',...
            'FontSize',12);
    else
        title(sprintf(...
            'Diffuse Field Response of simulated %s Mic Array', ...
            name_sanitized), ...
            'FontWeight','bold',...
            'FontSize',12);
    end
    
    % Create xlabel
    xlabel('Frequency (Hz x 10^4)', 'FontAngle', 'italic');
    % Create ylabel
    ylabel('Relative Response (dB)', 'FontAngle', 'italic');
    % Create legend
    legend1 = legend(axes1,'show');
    set(legend1,'Location','SouthWest');
end

function createfigure_ffr(M, xdir, X1,YMatrix1, fig)
    %  X1:  vector of x data
    %  YMATRIX1:  matrix of y data
    
    %  Auto-generated by MATLAB on 12-Mar-2011 11:02:34
    
    % Create figure
    if exist('fig', 'var')
        figure1 = fig;
    else
        figure1 = figure();
    end
    
    % Create axes
    axes1 = axes('Parent',figure1,'YGrid','on','XGrid','on',...
        'XMinorTick','on','YMinorTick','on','FontSize',12);
    box(axes1,'on');
    hold(axes1,'all');
    
    % Create multiple lines using matrix input to plot
    plot1 = plot(X1,YMatrix1,'Parent',axes1);
    ylim([-12,+2]);
    
    sig_names = 'WXYZ';
    for i = 1:size(YMatrix1,2)
        set(plot1(i),'DisplayName',sig_names(i));
    end
    
    % Create title
    name_sanitized = strrep(M.name, '_', '\_');
    if false
        title(sprintf(...
            ['Free Field Response of simulated %s Mic Array' ...
            '\n(r=%0.3fcm, \\alpha=%0.3f)'], ...
            name_sanitized, M.r*100, M.alpha),...
            'FontWeight','bold',...
            'FontSize',12);
    else
        [az,el,~] = cart2sph(xdir(1),xdir(2),xdir(3));
        title(sprintf(...
            'Free Field Response of simulated %s Mic Array az=%d, el=%d', ...
            name_sanitized, az, el), ...
            'FontWeight','bold',...
            'FontSize',12);
    end
    
    % Create xlabel
    xlabel('Frequency (Hz x 10^4)', 'FontAngle', 'italic');
    % Create ylabel
    ylabel('Relative Response (dB)', 'FontAngle', 'italic');
    % Create legend
    legend1 = legend(axes1,'show');
    set(legend1,'Location','SouthWest');
end
