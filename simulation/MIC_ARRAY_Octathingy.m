%% EMB's Octathingy
function M = MIC_ARRAY_Octathingy(radius, alpha1)
    % mic_array_octathingy return parameters for generic tetra microphone
    %  RADIUS is the acoustic radius of the array, typically 10% larger
    %  than physical radius.
    %  ALPHA1 is the gain of the first-order parameter
    %   0 = omni, 
    %   1/2 = cardioid, 
    %   1 = fig-8
    %   (3-sqrt(3))/2 = supercardioid (~0.63397)
    %   3/4 = hypercardoid
    %
    
    M.name = 'Octathingy';
    
    % The octathingy is an octahedron (capsules at the vertices of a cube),
    % with the top rotated 45deg, so it can do 2nd-order horizontal,
    % 1st-order vertical.
    
    if ~exist('radius', 'var')
        M.r = 1.47 * 1e-2; % meters
    else
        M.r = radius;
    end
    
    % capsule directivities
    %      for first-order mic: omni=[1,0]; cardioid=[0.5,0.5],fig8=[0,1];
    if ~exist('alpha1', 'var')
        alpha1 = 0.5;
    end
    
    M.alpha = [ ...
        1-alpha1 alpha1;
        1-alpha1 alpha1;
        1-alpha1 alpha1;
        1-alpha1 alpha1;
        1-alpha1 alpha1;
        1-alpha1 alpha1;
        1-alpha1 alpha1;
        1-alpha1 alpha1 ];
    
    % capsule orientations, direction cosines
    rt2 = sqrt(2);
    M.o = [[ ...
        % bottom ring
        -1  1 -1; % back-left-down .... LBD
        1  1 -1; % front-left-down
        +1 -1 -1; % front-right-down .. RFD
        -1 -1 -1 ]; % back-right-down
        
        [0 +rt2  1; % left-up
        +rt2  0  1; % front-up
        0 -rt2  1; % right-up
        -rt2  0  1]  % back-up
        ];
    % normalize
    M.o = diag(1./sqrt(diag(M.o * M.o'))) * M.o;
    
    % capsule positions for radial array, in meters
    M.u = M.o * M.r;
    
    % a2b matrix, Batke calls this the mode matrix, \Psi
    M.proj = M.alpha(:, [1,2,2,2]) .* [ones(size(M.o(:,1))), M.o];
    M.a2b = pinv(M.proj);
end

