
module cylinder_ep(p1, p2, radius) {
	vector = [p2[0] - p1[0],p2[1] - p1[1],p2[2] - p1[2]];
	distance = sqrt(pow(vector[0], 2) +	pow(vector[1], 2) +	pow(vector[2], 2));
	//echo(distance);
	translate(vector/2 + p1)
	//rotation of XoY plane by the Z axis with the angle of the [p1 p2] line projection with the X axis on the XoY plane
	rotate([0, 0, atan2(vector[1], vector[0])]) //rotation
	//rotation of ZoX plane by the y axis with the angle given by the z coordinate and the sqrt(x^2 + y^2)) point in the XoY plane
	rotate([0, atan2(sqrt(pow(vector[0], 2)+pow(vector[1], 2)),vector[2]), 0])
	cylinder(h = distance, r = radius, center = true);
}

//cylinder_ep([10,10,10],[-10,-10,10]);
//cylinder_ep([10,-10,-10], [-10,10,-10]);

//array_radius = 10;
//capsule_r    = 26.0/2;
capsule_h    = 4.6;

capsule_r    = 5;

echo(capsule_r);
array_radius = capsule_r/1.25;

ball_dist = 1.7*array_radius;
ball_radius = array_radius/3;

cap1_vec = [1,1,1]/sqrt(3);
cap2_vec = [-1,1,-1]/sqrt(3);
cap3_vec = [-1,-1,1]/sqrt(3);
cap4_vec = [1,-1,-1]/sqrt(3);

difference() {
 union() {
	cylinder_ep(array_radius * cap1_vec, (array_radius+capsule_h)*cap1_vec, capsule_r+1);

	cylinder_ep(array_radius * cap2_vec, (array_radius+capsule_h)*cap2_vec, capsule_r+1);

	cylinder_ep(array_radius * cap3_vec, (array_radius+capsule_h)*cap3_vec, capsule_r+1);

	cylinder_ep(array_radius * cap4_vec, (array_radius+capsule_h)*cap4_vec, capsule_r+1);

	translate([0,0,  ball_dist]) sphere(r=ball_radius);
	translate([0,0, -ball_dist]) sphere(r=ball_radius);

	translate([0,  ball_dist, 0]) sphere(r=ball_radius);
	translate([0, -ball_dist, 0]) sphere(r=ball_radius);

	translate([ ball_dist, 0,0]) sphere(r=ball_radius);
	translate([-ball_dist, 0,0]) sphere(r=ball_radius);

	translate([0,0,-20]) cylinder(h=30,r=1,center=true);

	rotate([90,0,0]) translate([0,0,ball_dist]) cylinder(h=30,r=1);
	rotate([-90,0,0]) translate([0,0,ball_dist]) cylinder(h=30,r=1);

	rotate([0,90,0]) translate([0,0,ball_dist]) cylinder(h=30,r=1);
	rotate([0,-90,0]) translate([0,0,ball_dist]) cylinder(h=30,r=1);
 };
 union(){
    cylinder_ep((array_radius-3) * cap1_vec, (array_radius+capsule_h+1)*cap1_vec, capsule_r);
    cylinder_ep((array_radius-3) * cap2_vec, (array_radius+capsule_h+1)*cap2_vec, capsule_r);
    cylinder_ep((array_radius-3) * cap3_vec, (array_radius+capsule_h+1)*cap3_vec, capsule_r);
    cylinder_ep((array_radius-3) * cap4_vec, (array_radius+capsule_h+1)*cap4_vec, capsule_r);
 }
}


