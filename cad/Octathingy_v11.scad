cap_d = 10;
cap_h = 4.5;
module annulus(az,el,r) 
	rotate([0,0,az])
	  rotate([0,90-el,0])
		translate([0,0,r])
			difference() {
	 			cylinder(h=cap_h,   r=cap_d/2+1, center=false, $fn=36);
	 			//translate([0,0,-1/2]) cylinder(h=cap_h+1, r=cap_d/2,   center=false);
			}

module annulus2(az,el,r) 
	rotate([0,0,az])
	  rotate([0,90-el,0])
		translate([0,0,r])
			difference() {
	 			//cylinder(h=cap_h,   r=cap_d/2+1, center=false);
	 			translate([0,0,-1/2])
        				cylinder(h=cap_h+1, r=cap_d/2,   center=false, $fn=36);
			}


module vertical(az, r)
	rotate([0,0,az])
		translate([r,0,0])
 			cylinder(h=14,r=1.5,center=true,$fn=12);

difference() {
  union() {
		for( az=[0:45:315] )
			annulus(az,atan(sqrt(1/2)),15);


		for( az=[0:45:315] )
			annulus(az,-atan(sqrt(1/2)),15);

		for( az=[0:45:315] )
			vertical(az,17.25);

	    //cylinder(h=30,r=2,center=false);
  }
  for( az=[0:45:315] )
			annulus2(az,atan(sqrt(1/2)),15);


  for( az=[0:45:315] )
			annulus2(az,-atan(sqrt(1/2)),15);
}



//sphere(r=15);